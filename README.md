# Streamlit 在 [21云盒子](https://www.21cloudbox.com/) 的示例2

这个示例使用了: `matplotlib`, `pandas`, `scipy` 和 `Pillow`｡

这是 [21云盒子](https://www.21cloudbox.com/) 上创建的 [Streamlit](https://www.streamlit.io/) 示例｡


## 部署方式

详情看 [https://www.21cloudbox.com/blog/solutions/how-to-deploy-streamlit-project-in-production-server.html](https://www.21cloudbox.com/blog/solutions/how-to-deploy-streamlit-project-in-production-server.html)｡

*原代码来自: https://github.com/antonio-catalano/RationalPassiveInvesting*